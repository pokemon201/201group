package guiInterface;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Dialog extends JDialog
{
	private static final long serialVersionUID = 1L;
	public Dialog(JFrame parent, String title, String msg)
	{
		super(parent,title);
		setLocation(700,500);
		JPanel mp=new JPanel();
		JPanel bp=new JPanel();
		JButton ok=new JButton("OK");
		ok.addActionListener(new MyActionListener());
		mp.add(new JLabel(msg));
		bp.add(ok);
		getContentPane().add(mp);
		getContentPane().add(bp,BorderLayout.PAGE_END);
		pack();
		setVisible(true);	
	}
	  class MyActionListener implements ActionListener 
	  { 	  
		  public void actionPerformed(ActionEvent e) 
		  {
			  setVisible(false);
			  dispose();
		  } 
	  }
}
