package guiInterface;

import guiInterface.Rooms.ChatClientReceiver;
import guiInterface.Rooms.ChatClientSender;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultCaret;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class InRoom extends JFrame 
{
	private JLabel title, myTeam, icon1, icon2, icon3, opponent, oicon1, oicon2, oicon3; 
	private JPanel up, left, right, center, south, icons, oicons, buttons, south_container; 
	private JTextArea stats;
	private JTextField newChat;
	private JButton leave_room, ready, enter, play; 
	private boolean gameEnd = false;
	private boolean win = false;
	private static final long serialVersionUID = 1L;
	ImageIcon bg = new ImageIcon(getClass().getResource("/images/roombg.jpg"));
	Image bgImage = bg.getImage();
	private int roomNumber;
	private ArrayList<Pokemon> pkm;
	private ArrayList<Pokemon> oppPkm;
	private boolean guest;
	private String roomType[] = {"Ground", "Water", "Grass", "Fire", "Electric"};
	private final ImageIcon roomIcons[];
	private ReentrantLock lock = new ReentrantLock();
	volatile boolean readyPressed = false;
	private java.applet.AudioClip click = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/Do.WAV"));
	String playername;
	
	private Connection conn;
	private Statement st;
	private ResultSet rs;
	boolean enterPressed = false;
	
	{
		roomIcons = new ImageIcon[5];
		roomIcons[0] = new ImageIcon(getClass().getResource("/images/room1.jpg"));
		roomIcons[1] = new ImageIcon(getClass().getResource("/images/room2.jpg"));
		roomIcons[2] = new ImageIcon(getClass().getResource("/images/room3.jpg"));
		roomIcons[3] = new ImageIcon(getClass().getResource("/images/room4.jpg"));
		roomIcons[4] = new ImageIcon(getClass().getResource("/images/room5.jpg"));
	}
	private final ImageIcon pokemonIcons[];
	{
		pokemonIcons = new ImageIcon[20];
		pokemonIcons[0] = new ImageIcon(getClass().getResource("/images/1.png"));
		pokemonIcons[1] = new ImageIcon(getClass().getResource("/images/2.png"));
		pokemonIcons[2] = new ImageIcon(getClass().getResource("/images/3.png"));
		pokemonIcons[3] = new ImageIcon(getClass().getResource("/images/4.png"));
		pokemonIcons[4] = new ImageIcon(getClass().getResource("/images/5.png"));
		pokemonIcons[5] = new ImageIcon(getClass().getResource("/images/6.png"));
		pokemonIcons[6] = new ImageIcon(getClass().getResource("/images/7.png"));
		pokemonIcons[7] = new ImageIcon(getClass().getResource("/images/8.png"));
		pokemonIcons[8] = new ImageIcon(getClass().getResource("/images/9.png"));
		pokemonIcons[9] = new ImageIcon(getClass().getResource("/images/10.png"));
		pokemonIcons[10] = new ImageIcon(getClass().getResource("/images/11.png"));
		pokemonIcons[11] = new ImageIcon(getClass().getResource("/images/12.png"));
		pokemonIcons[12] = new ImageIcon(getClass().getResource("/images/13.png"));
		pokemonIcons[13] = new ImageIcon(getClass().getResource("/images/14.png"));
		pokemonIcons[14] = new ImageIcon(getClass().getResource("/images/15.png"));
		pokemonIcons[15] = new ImageIcon(getClass().getResource("/images/16.png"));
		pokemonIcons[16] = new ImageIcon(getClass().getResource("/images/17.png"));
		pokemonIcons[17] = new ImageIcon(getClass().getResource("/images/18.png"));
		pokemonIcons[18] = new ImageIcon(getClass().getResource("/images/19.png"));
		pokemonIcons[19] = new ImageIcon(getClass().getResource("/images/20.png"));
	}
	private ImageIcon[] myIcons = new ImageIcon[3];
	private ImageIcon[] opIcons = new ImageIcon[3];
	
	public class myPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		@Override
		protected void paintComponent(Graphics g) 
		{
		    super.paintComponent(g);
		        g.drawImage(bgImage, 0, 0, null);
		}
	}
	
	class ChatClientSender extends Thread {
		private PrintWriter pwpw;
		private Socket ss;
		private BufferedReader brbr;
		public void run() {
			try {
				ss = new Socket("localhost", 6789);
				pwpw = new PrintWriter(ss.getOutputStream());
				brbr =  new BufferedReader(new InputStreamReader(ss.getInputStream()));
				ChatClientReceiver ccr = new ChatClientReceiver(brbr,ss);
				ccr.start();
				while (true) {
					//lock.lock();
					Thread.sleep(40);
					if (enterPressed) {
						pwpw.println("^"+roomNumber+ playername +": "+newChat.getText());
						newChat.setText("");
						pwpw.flush();
						enterPressed = false;
					}
					//lock.unlock();
				}
			} catch (IOException ioe) {
				System.out.println("IOE in ChatClientSender.run(): " + ioe.getMessage());
			} catch (InterruptedException e) {
				
			} finally {
				try {
					if (pwpw != null) pwpw.close();
					if (ss != null) ss.close();
				} catch (Exception e) {
					System.out.println("Exception in ChatClientSender: " + e.getMessage());
				}
			}
		}
	}
	
	class ChatClientReceiver extends Thread {

		private BufferedReader brbr;
		private Socket ss;
		//int i = 0;
		ChatClientReceiver (BufferedReader brbr, Socket ss){
			this.brbr = brbr;
			this.ss = ss;
		}
		public void run() {
			try {
				while (true) {
					//Thread.sleep(40);
					String line = brbr.readLine();
					if (line.charAt(line.length()-2)!=':') {
						stats.append( line+"\n");
						}
				}
			} catch (IOException ioe) {
				System.out.println("IOE in ChatClientReceiver.run(): " + ioe.getMessage());
			}  finally {
				try {
					if (brbr != null) brbr.close();
					if (ss != null) ss.close();
				} catch (Exception e) {
					System.out.println("Exception in ChatClientReceiver: " + e.getMessage());
				}
			}
		}
	}
	
	
	
	
	
	class bothReadyChecker extends Thread {
		private PrintWriter pw;
		BufferedReader br;
		private Socket s;
		public void run() {
			try {
				s = new Socket("localhost", 6789);
				pw = new PrintWriter(s.getOutputStream());
				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				bothReady btr = new bothReady(br, s);
				btr.start();
				while (true) {
					//lock.lock();
					Thread.sleep(40);
					if (readyPressed) {
						pw.println("#"+"\t"+roomNumber+"\t"+playername+"\t"+pkm.get(0).getID()+"\t"+pkm.get(1).getID()+"\t"+pkm.get(2).getID());
						pw.flush();
						readyPressed = false;
						
						break;
					}
					//lock.unlock();
				}
				
			}catch (IOException ioe) {
				System.out.println("IOE in Inroom ready button " + ioe.getMessage());
			} catch (InterruptedException e) {
				
			} finally {
				
			}
		}
	}
	
	
	public class bothReady extends Thread {
		private BufferedReader br;
		private Socket s;
		bothReady (BufferedReader br, Socket s){
			this.br = br;
			this.s = s;
		}
		public void run() {
			try {
				
				while (true) {
					String line = br.readLine();
					if (line != null && line != "null") {
						String [] str = line.split("\t");
						if (str[0].equals("bothReady")) {
							play.setEnabled(true);
							//initialize opponent pokemon
							initializeOppPkm(Integer.parseInt(str[1]), Integer.parseInt(str[2]), Integer.parseInt(str[3]));
							break;
						}
					}
				}
			} catch (IOException ioe) {
				System.out.println("IOE in bothready.run(): " + ioe.getMessage());
			} finally {
				try {
					if (br != null) br.close();
					if (s != null) s.close();
				} catch (Exception e) {
					System.out.println("Exception in bothready: " + e.getMessage());
				}
			}
		}
		
		private void initializeOppPkm(int pkm1, int pkm2, int pkm3) {
			int[] indices = new int[3];
			indices[0] = pkm1;
			indices[1] = pkm2;
			indices[2] = pkm3;
			
			for ( int index : indices) {
				System.out.println("Index: "+index);
				Skill tmpS[]=new Skill[4];
				Pokemon tmpP=null;
				try {
					Class.forName("com.mysql.jdbc.Driver");
					conn = DriverManager.getConnection("jdbc:mysql://localhost/PokemonDatabase?user=root");
					st = conn.createStatement();
					String sql = "SELECT * FROM Pokemon WHERE pokemonID = " + (index);
					rs = st.executeQuery(sql);

					while (rs.next()) {
						int [] skills = new int[3];
						skills[0] = Integer.parseInt(rs.getString(6));
						skills[1] = Integer.parseInt(rs.getString(7));
						skills[2] = Integer.parseInt(rs.getString(8));
						tmpS[0] = new Skill("Attack","Normal",3,0);
						for ( int i=1; i<4; i++ ) {
							sql = "SELECT * FROM Skill WHERE skillID = " + skills[i-1];
							rs = st.executeQuery(sql);
							while (rs.next()) {
								tmpS[i] = new Skill (rs.getString(2),rs.getString(7),rs.getInt(3),rs.getInt(4));

							}
						}
					}
					sql = "SELECT * FROM Pokemon WHERE pokemonID = " + (index);
					rs = st.executeQuery(sql);
					while (rs.next()) {
						tmpP=new Pokemon(rs.getString(2),rs.getInt(4),rs.getInt(5),10,
								rs.getString(3),tmpS,pokemonIcons[index-1],index);
						oppPkm.add(tmpP);
					}
				} catch (SQLException sqle) {
					System.out.println ("SQLException: " + sqle.getMessage());
				} catch (ClassNotFoundException cnfe) {
					System.out.println ("ClassNotFoundException: " + cnfe.getMessage());
				} finally {
					try { if (rs != null) rs.close(); } catch (SQLException er) { er.printStackTrace(); }
					try { if (st != null) st.close(); } catch (SQLException er) { er.printStackTrace(); }
					try { if (conn != null) conn.close(); } catch (SQLException er) { er.printStackTrace(); }
				}
			
			
			}
			setOpponentTeam();
		}
	}
	
	
	
	
	public InRoom(int n, ArrayList<Pokemon> pkm, boolean end, boolean w, boolean ifGuest, String playername) 
	{
		super("Pokemon");
		
		this.pkm=pkm;
		this.playername = playername;
////////for testting//hard coded opponent
		oppPkm=new ArrayList<Pokemon>();
		/*Skill tmpS[]=new Skill[4];
		tmpS[0]=new Skill("Move1","Water",10,5);
		tmpS[1]=new Skill("Move2","Fire",11,5);
		tmpS[2]=new Skill("Move3","Electric",12,4);
		tmpS[3]=new Skill("Move4","Grass",13,7);
		Pokemon pm1=new Pokemon("desk",100,100,10,"Water",tmpS,pokemonIcons[4],5);
		Pokemon pm2=new Pokemon("Table",100,100,10,"Water",tmpS,pokemonIcons[8],9);
		Pokemon pm3=new Pokemon("RoundTable",100,100,10,"Water",tmpS,pokemonIcons[11],12);
		oppPkm.add(pm1);
		oppPkm.add(pm2);
		oppPkm.add(pm3);*/
		///////fr testting//
		roomNumber = n;
		gameEnd = end;
		win = w;
		guest = ifGuest;
		setSize(new Dimension(800, 660));
		getContentPane().setLayout(
				new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		stats = new JTextArea(6, 60);
		newChat = new JTextField();
		stats.setText("");
		JScrollPane scroll = new JScrollPane(stats);
		scroll.setPreferredSize(new Dimension(750, 150));
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		DefaultCaret carett = (DefaultCaret)stats.getCaret();
		carett.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		Thread tst = new ChatClientSender();
		tst.start();
		
		left= new JPanel();
		left.setOpaque(false);
		right= new JPanel();
		right.setOpaque(false);
		center= new JPanel();
		center.setOpaque(false);
		south= new JPanel();
		south.setOpaque(false);
		icons= new JPanel();
		icons.setOpaque(false);
		oicons= new JPanel();
		oicons.setOpaque(false);
		buttons= new JPanel();
		buttons.setOpaque(false);
		buttons.setPreferredSize(new Dimension(750, 30));
		icons.setPreferredSize(new Dimension(390, 250));
		icons.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		/////ocions 
		ImageIcon lr;
		if(!gameEnd)
			lr = new ImageIcon(getClass().getResource("/images/leave.jpg"));
		else
			lr = new ImageIcon(getClass().getResource("/images/endgame.jpg"));
		leave_room= new JButton(lr); 
		leave_room.setPreferredSize(new Dimension(115, 25));
		leave_room.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				click.play();
				setVisible(false);
				if(!gameEnd)
					new Rooms(pkm,guest,playername); 
				else//endgame
					System.exit(0);
            }
		});
		ImageIcon rd;
		if(!gameEnd)
			rd = new ImageIcon(getClass().getResource("/images/ready.jpg"));
		else
			rd = new ImageIcon(getClass().getResource("/images/blank.jpg"));
		ready= new JButton(rd); 
		ready.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
            {
				click.play();
				readyPressed = true;
            }
		});
		ImageIcon pl;
		if(!gameEnd)
			pl = new ImageIcon(getClass().getResource("/images/play.jpg"));
		else
			pl = new ImageIcon(getClass().getResource("/images/newgame.jpg"));
		Thread tt = new bothReadyChecker();
		tt.start();
		play= new JButton(pl);
		if(!gameEnd)
			play.setEnabled(false);
		
		play.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
            {
				click.play();
					setVisible(false);
					if(!gameEnd)
					{
						new battleroom(roomNumber,pkm,oppPkm,guest,roomType[roomNumber],playername);
					}
					if(gameEnd) 
					{
						gameEnd = false;
						if(guest) new Rooms(pkm, true,playername);
						else new Team(playername);
					}
            }
		});
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		ready.setPreferredSize(new Dimension(115, 25));
		
		
	
		play.setPreferredSize(new Dimension(115, 25));
		buttons.add(Box.createGlue());
		buttons.add(leave_room);
		buttons.add(Box.createGlue()); 
		buttons.add(ready);
		buttons.add(Box.createGlue()); 
		buttons.add(play); 
		buttons.add(Box.createGlue()); 		
		
		//right.add(buttons, BorderLayout.SOUTH);
		up= new JPanel();
		up.setOpaque(false);
		up.setPreferredSize(new Dimension(800, 100));
		ImageIcon roomTitle;
		if(!gameEnd)
			roomTitle = roomIcons[roomNumber];
		else
		{
			if(win)
				roomTitle = new ImageIcon(getClass().getResource("/images/youwon.jpg"));
			else
				roomTitle = new ImageIcon(getClass().getResource("/images/youlose.jpg"));
		}
		title= new JLabel(roomTitle, SwingConstants.CENTER); 
		title.setPreferredSize(new Dimension(700, 90));
		left.setPreferredSize(new Dimension(350, 330));
		up.add(title);
		
		right.setPreferredSize(new Dimension(350, 330));
		
		center.setPreferredSize(new Dimension(800, 330));
		setMyTeam();
		//setOpponentTeam();
		south_container =new JPanel();
		//south_container.setOpaque(false);
		south_container.setPreferredSize(new Dimension(750, 110));
		south.setPreferredSize(new Dimension(750, 100));
		center.setLayout(new BoxLayout(center, BoxLayout.X_AXIS));
		center.add(left);
		center.add(right);
		south.setLayout(new BoxLayout(south, BoxLayout.Y_AXIS));
		south.add(scroll); 
		JPanel bottom = new JPanel(); 
		bottom.setOpaque(false);
		bottom.setPreferredSize(new Dimension(700, 100));
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
		bottom.add (newChat);
		enter= new JButton("Enter"); 
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enterPressed = true;
			}
		});
		enter.doClick();
		bottom.add(enter);
		south.add(bottom); 
		south_container.add(south, BorderLayout.SOUTH);
		myPanel mp = new myPanel();
		mp.add(up );
		mp.add(center);
		mp.add(buttons); 
		mp.add(south_container);
		add(mp);
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	

	public void setMyTeam()
	{
		myIcons[0] = pkm.get(0).getImc();
		myIcons[1] = pkm.get(1).getImc();
		myIcons[2] = pkm.get(2).getImc();
		icon1= new JLabel(myIcons[0]);
		icon2= new JLabel(myIcons[1]);
		icon3= new JLabel(myIcons[2]);
		icon1.setPreferredSize(new Dimension(130, 230));
		icon2.setPreferredSize(new Dimension(120, 230));
		icon3.setPreferredSize(new Dimension(120, 230));
		icons.add(icon1);
		icons.add(icon2);
		icons.add(icon3);
		//icons.setBackground(new Color(224,255,255));
		ImageIcon mt = new ImageIcon(getClass().getResource("/images/myteam.jpg"));
		myTeam= new JLabel(mt);
		myTeam.setPreferredSize(new Dimension(350, 30));
		myTeam.setFont(new Font("Serif", Font.PLAIN, 25));
		left.add(myTeam, BorderLayout.NORTH); 
		left.add(icons, BorderLayout.CENTER);
		////can be modify based on situations 
	}
	public void setOpponentTeam()
	{
		oicons.setPreferredSize(new Dimension(390, 250));
		oicons.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		//oicons.setBackground(new Color(224,255,255));
		opIcons[0] = oppPkm.get(0).getImc();
		opIcons[1] = oppPkm.get(1).getImc();
		opIcons[2] = oppPkm.get(2).getImc();
		oicon1= new JLabel(opIcons[0]);
		oicon2= new JLabel(opIcons[1]);
		oicon3= new JLabel(opIcons[2]);
		oicon1.setPreferredSize(new Dimension(110, 230));
		oicon2.setPreferredSize(new Dimension(110, 230));
		oicon3.setPreferredSize(new Dimension(110, 230));
		oicons.add(oicon1);
		oicons.add(oicon2);
		oicons.add(oicon3);
		ImageIcon op = new ImageIcon(getClass().getResource("/images/opponent.jpg"));
		opponent= new JLabel(op);
		opponent.setPreferredSize(new Dimension(350, 30));
		right.add(opponent, BorderLayout.NORTH); 
		right.add(oicons, BorderLayout.CENTER);		
		this.revalidate();
		this.repaint();
	}

}
