package guiInterface;

import java.io.Serializable;

import javax.swing.ImageIcon;

public class Pokemon
{
	private String name;
	private ImageIcon imc;
	private int hp;
	private int mp;
	//private int defense;
	//private int speed;
	private int attack; 
	private String type; 
	private Skill skills[];
	private boolean dead;
	private int sqlID;
	public Pokemon(String name,int hp,int mp,/**int defense,int speed,*/int attack,String type, Skill skills[],ImageIcon imc,int sqlID)
	{
		this.name=name;
		this.hp=hp;
		this.mp=mp;
		this.sqlID = sqlID;
		//this.defense=defense;
		//this.speed=speed;
		this.attack=attack;
		this.type=type;
		this.skills=skills;
		this.imc=imc;
		dead = false;
	}
	public ImageIcon getImc()
	{
		return imc;
	}
	public String getName()
	{
		return name;
	}
	public int getHp()
	{
		return hp;
	}
	public int getMp()
	{
		return mp;
	}
	
	public void takeDmg(int dmg)
	{
		hp-=dmg;
		if(hp <= 0)
		{
			hp = 0;
			dead = true;
		}
	}
	
	public void cast(Skill sk)
	{
		mp-=sk.getMp();
	}
	
	public int getAttack()
	{
		return attack;
	}
	public String getType()
	{
		return type;
	}
	public Skill getSkill(int index)
	{
		return skills[index];
	}
	public boolean isDead()
	{
		return dead;
	}
	public int getID() {
		return sqlID;
	}
	
}
