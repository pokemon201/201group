package guiInterface;

import guiInterface.InRoom.ChatClientSender;
import guiInterface.InRoom.bothReady;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.DefaultCaret;

public class Rooms extends JFrame 
{
	private java.applet.AudioClip click = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/Do.WAV"));
	private java.applet.AudioClip roomChosen = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/chosenRoom.WAV"));

	private JLabel title, myTeam, icon1, icon2, icon3; 
	private JPanel left, right, center, south, icons, buttons, south_container; 
	JTextArea stats; 
	JTextField newchat;
	JScrollPane scroll; 
	private JTable table;
	private int roomNum = -1;
	JButton rebuild_team, enter_room, enter; 
	@SuppressWarnings("unused")
	private ArrayList<Pokemon> pkm;
	private boolean guest;
	private boolean enterPressed;
	private static final long serialVersionUID = 1L;
	private String playername;
	private ReentrantLock lock = new ReentrantLock();
	Vector<String> playerVec = new Vector<String> (5);
	
	
	class askPlayers extends Thread {
		PrintWriter pwpw;
		BufferedReader brbr;
		Socket sss;
		public void run(){
			try {
				sss = new Socket("localhost", 6789);
				pwpw = new PrintWriter(sss.getOutputStream());
				brbr = new BufferedReader(new InputStreamReader(sss.getInputStream()));
				pwpw.println("$");
				pwpw.flush();
				String line;
				while (true) {
					line = brbr.readLine();
					
					System.out.println("from server:"+ line);
					break;
				}
				String [] str = line.split("\t");
				for (int i = 0;i<5;i++) {
					System.out.println(str[i]);
					playerVec.add(str[i]);
				}
					
				initializeTable();
				
				
			} catch (IOException ioe) {
				System.out.println("IOE in Inroom ready button " + ioe.getMessage());
			} finally {
				
			}
		}
	
	}
	
	
	
	
	public void initializeTable(){
		//System.out.println("Initialize the table");
		String[] columnNames = {"#Room","Players"};
		
		Object[][] data = {
				{" Room 1", playerVec.get(0)},
				{" Room 2", playerVec.get(1)},
				{" Room 3", playerVec.get(2)},
				{" Room 4", playerVec.get(3)},
				{" Room 5", playerVec.get(4)}};
		System.out.println("test"+playerVec.get(0)+playerVec.get(1));
		
		
		table = new JTable(data, columnNames){
			private static final long serialVersionUID = 1L;
		      public boolean isCellEditable(int row, int column){  
		          return false;  
		        }  
		      public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
			        Component returnComp = super.prepareRenderer(renderer, row, column);
			        Color alternateColor = new Color(209,230,231);
			        Color whiteColor = Color.WHITE;
			        if (!returnComp.getBackground().equals(getSelectionBackground())){
			            Color bg = (row % 2 == 0 ? alternateColor : whiteColor);
			            returnComp .setBackground(bg);
			            bg = null;
			        }
			        return returnComp;
			 }
		};
		table.getTableHeader().setFont(new Font("Verdana", Font.BOLD, 15));
		table.setFont(new Font("Arial", Font.BOLD, 12));
		
		table.addMouseListener(tableMouseListener);

		JScrollPane scrollPane = new JScrollPane(table);
		table.setPreferredScrollableViewportSize(new Dimension(300, 400));
		
		TableColumn column;
		for (int i = 0; i < 2; i++) {
		    column = table.getColumnModel().getColumn(i);
		    if (i == 0) {
		        column.setPreferredWidth(100); 
		    } else {
		        column.setPreferredWidth(200);
		    }
		}
		for (int i= 0; i < 5; i++) {
			table.setRowHeight(i, 30);}
		
		left.add(scrollPane);
		
		table.revalidate();
		table.repaint();
	}
	
	
	class ChatClientSender extends Thread {
		private PrintWriter pw;
		private Socket s;
		public void run() {
			try {
				s = new Socket("localhost", 6789);
				pw = new PrintWriter(s.getOutputStream());
				
				ChatClientReceiver ccr = new ChatClientReceiver();
				ccr.start();
				while (true) {
					lock.lock();
					Thread.sleep(40);
					if (enterPressed) {
						pw.println(playername +": "+newchat.getText());
						newchat.setText("");
						pw.flush();
						enterPressed = false;
					}
					lock.unlock();
				}
			} catch (IOException ioe) {
				System.out.println("IOE in ChatClientSender.run(): " + ioe.getMessage());
			} catch (InterruptedException e) {
				
			} finally {
				try {
					if (pw != null) pw.close();
					if (s != null) s.close();
				} catch (Exception e) {
					System.out.println("Exception in ChatClientSender: " + e.getMessage());
				}
			}
		}
	}
	
	class ChatClientReceiver extends Thread {

		private BufferedReader br;
		private Socket s;

		public void run() {
			try {
				s = new Socket("localhost", 6789);
				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				
				while (true) {
					String line = br.readLine();
					if (line.charAt(line.length()-2)!=':') {
						stats.append( line+"\n");
						}
				}
			} catch (IOException ioe) {
				System.out.println("IOE in ChatClientReceiver.run(): " + ioe.getMessage());
			} finally {
				try {
					if (br != null) br.close();
					if (s != null) s.close();
				} catch (Exception e) {
					System.out.println("Exception in ChatClientReceiver: " + e.getMessage());
				}
			}
		}
	}
	
	
	
	
	
	private final ImageIcon pokemonIcons[];
	{
		pokemonIcons = new ImageIcon[20];
		pokemonIcons[0] = new ImageIcon(getClass().getResource("/images/1.png"));
		pokemonIcons[1] = new ImageIcon(getClass().getResource("/images/2.png"));
		pokemonIcons[2] = new ImageIcon(getClass().getResource("/images/3.png"));
		pokemonIcons[3] = new ImageIcon(getClass().getResource("/images/4.png"));
		pokemonIcons[4] = new ImageIcon(getClass().getResource("/images/5.png"));
		pokemonIcons[5] = new ImageIcon(getClass().getResource("/images/6.png"));
		pokemonIcons[6] = new ImageIcon(getClass().getResource("/images/7.png"));
		pokemonIcons[7] = new ImageIcon(getClass().getResource("/images/8.png"));
		pokemonIcons[8] = new ImageIcon(getClass().getResource("/images/9.png"));
		pokemonIcons[9] = new ImageIcon(getClass().getResource("/images/10.png"));
		pokemonIcons[10] = new ImageIcon(getClass().getResource("/images/11.png"));
		pokemonIcons[11] = new ImageIcon(getClass().getResource("/images/12.png"));
		pokemonIcons[12] = new ImageIcon(getClass().getResource("/images/13.png"));
		pokemonIcons[13] = new ImageIcon(getClass().getResource("/images/14.png"));
		pokemonIcons[14] = new ImageIcon(getClass().getResource("/images/15.png"));
		pokemonIcons[15] = new ImageIcon(getClass().getResource("/images/16.png"));
		pokemonIcons[16] = new ImageIcon(getClass().getResource("/images/17.png"));
		pokemonIcons[17] = new ImageIcon(getClass().getResource("/images/18.png"));
		pokemonIcons[18] = new ImageIcon(getClass().getResource("/images/19.png"));
		pokemonIcons[19] = new ImageIcon(getClass().getResource("/images/20.png"));
	}
	
	MouseListener tableMouseListener = new MouseAdapter() {
	      @Override
	      public void mouseClicked(MouseEvent e) 
	      {
	    	  click.play();
	         roomNum = table.getSelectedRow();
	      }
	   };
	
	   
	public Rooms(ArrayList<Pokemon> pkm, boolean ifGuest, String playername) 
	{
		super("Pokemon Rooms");
		this.pkm=pkm;
		guest = ifGuest;
		this.playername = playername;
		setSize(new Dimension(800, 660));
			ImageIcon bgi = new ImageIcon(getClass().getResource("/images/roomlistbg.jpg"));
		    final Image backgroundImage = bgi.getImage();
		    setContentPane(new JPanel(new BorderLayout()) {
		    	private static final long serialVersionUID = 1L;
		        @Override public void paintComponent(Graphics g) {
		            g.drawImage(backgroundImage, 0, 0, null);
		        }
		    });
		///scroll  pane 
		Thread ask = new askPlayers();
		ask.start();
		stats = new JTextArea(4, 60);
		stats.setEditable(false);
		DefaultCaret caret = (DefaultCaret)stats.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		newchat = new JTextField(); newchat.setOpaque(true);
		stats.setText("");
		JScrollPane scroll = new JScrollPane(stats);
		scroll.setMaximumSize(new Dimension(800, 40));
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		left= new JPanel(); left.setOpaque(false);
		right= new JPanel(); right.setOpaque(false);
		center= new JPanel(); center.setOpaque(false);
		south= new JPanel(); south.setOpaque(false);
		icons= new JPanel(); icons.setOpaque(false);
		buttons= new JPanel(); buttons.setOpaque(false);
		buttons.setPreferredSize(new Dimension(350, 40));
		icons.setPreferredSize(new Dimension(350, 250));
		icon1= new JLabel(pkm.get(0).getImc());
		icon2= new JLabel(pkm.get(1).getImc());
		icon3= new JLabel(pkm.get(2).getImc());
		icon1.setPreferredSize(new Dimension(110, 230));
		icon2.setPreferredSize(new Dimension(110, 230));
		icon3.setPreferredSize(new Dimension(110, 230));
		icons.add(icon1);
		icons.add(icon2);
		icons.add(icon3);
		ImageIcon er = new ImageIcon(getClass().getResource("/images/enterroom.jpg"));
		ImageIcon rb = new ImageIcon(getClass().getResource("/images/rebuild.jpg"));
		rebuild_team= new JButton(rb); 
		rebuild_team.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
            {
				click.play();
				setVisible(false);
				new Team(playername);
            }
		});
		enter_room= new JButton(er); 
		enter_room.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				if(roomNum != -1)
				{
					roomChosen.play();
					setVisible(false);
					new InRoom(roomNum,pkm,false,false,guest,playername);
				}
				else
				{
					click.play();
					JOptionPane.showMessageDialog(Rooms.this,"Select room first",
							"Error",JOptionPane.WARNING_MESSAGE);
				}
            }
		});
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		enter_room.setPreferredSize(new Dimension(150, 30));
		rebuild_team.setPreferredSize(new Dimension(150, 30));
		if(guest)
			rebuild_team.setEnabled(false);
		buttons.add(Box.createGlue()); 
		buttons.add(enter_room);
		buttons.add(Box.createGlue()); 
		buttons.add(rebuild_team);
		buttons.add(Box.createGlue()); 
		JPanel empty= new JPanel();	empty.setOpaque(false);
		empty.setPreferredSize(new Dimension(800, 20));
		ImageIcon mt = new ImageIcon(getClass().getResource("/images/myteam.jpg"));
		myTeam= new JLabel(mt);
		myTeam.setPreferredSize(new Dimension(350, 30));
		myTeam.setFont(new Font("Serif", Font.PLAIN, 15));
		right.add(myTeam, BorderLayout.NORTH); 
		right.add(icons, BorderLayout.CENTER);
		right.add(buttons, BorderLayout.SOUTH);
		title= new JLabel("     ",SwingConstants.CENTER); 
		title.setFont(new Font("Serif", Font.PLAIN, 55));
		title.setPreferredSize(new Dimension(800, 120));
		left.setPreferredSize(new Dimension(350, 350));
		
		/*String[] columnNames = {"#Room","Players"};

		
		
		
		
		
		
		
		
		
		
		
		
		
		Object[][] data = {
				{" Room 1", playerVec.get(0)},
				{" Room 2", playerVec.get(1)},
				{" Room 3", playerVec.get(2)},
				{" Room 4", playerVec.get(3)},
				{" Room 5", playerVec.get(4)}};
		
		
		
		table = new JTable(data, columnNames){
			private static final long serialVersionUID = 1L;
		      public boolean isCellEditable(int row, int column){  
		          return false;  
		        }  
		      public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
			        Component returnComp = super.prepareRenderer(renderer, row, column);
			        Color alternateColor = new Color(209,230,231);
			        Color whiteColor = Color.WHITE;
			        if (!returnComp.getBackground().equals(getSelectionBackground())){
			            Color bg = (row % 2 == 0 ? alternateColor : whiteColor);
			            returnComp .setBackground(bg);
			            bg = null;
			        }
			        return returnComp;
			 }
		};
		table.getTableHeader().setFont(new Font("Verdana", Font.BOLD, 15));
		table.setFont(new Font("Arial", Font.BOLD, 12));
		
		table.addMouseListener(tableMouseListener);

		JScrollPane scrollPane = new JScrollPane(table);
		table.setPreferredScrollableViewportSize(new Dimension(300, 400));
		
		TableColumn column;
		for (int i = 0; i < 2; i++) {
		    column = table.getColumnModel().getColumn(i);
		    if (i == 0) {
		        column.setPreferredWidth(100); 
		    } else {
		        column.setPreferredWidth(200);
		    }
		}
		for (int i= 0; i < 5; i++) {
			table.setRowHeight(i, 30);}
		left.add(scrollPane);*/

		right.setPreferredSize(new Dimension(350, 350));
		center.setPreferredSize(new Dimension(800, 350));
		south_container =new JPanel(); south_container.setOpaque(false);
		south_container.setPreferredSize(new Dimension(800, 120));
		south.setPreferredSize(new Dimension(750, 100));
		center.setLayout(new BoxLayout(center, BoxLayout.X_AXIS));
		center.add(left);
		center.add(right);
		south.setLayout(new BoxLayout(south, BoxLayout.Y_AXIS));
		south.add(scroll); 
		JPanel bottom = new JPanel(); bottom.setOpaque(false);
		bottom.setPreferredSize(new Dimension(800, 30));
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
		bottom.add (newchat);
		enter= new JButton("Enter"); 
		enter.setOpaque(true);
		bottom.add(enter);
		south.add(bottom); 
		south.add(empty);
		south_container.add(south, BorderLayout.CENTER);
		
		add(title, BorderLayout.NORTH);
		add(south_container, BorderLayout.SOUTH);
		add(center, BorderLayout.CENTER);
		
		enterPressed = false;
		addListeners();
		//////////////////////////////
		Thread tt = new ChatClientSender();
		tt.start();
		setResizable(false);
		setLocationRelativeTo(null);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	}
	
	private void addListeners() {
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enterPressed = true;
			}
		});
		
		newchat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enterPressed = true;
			}
		});
	}
}
