package guiInterface;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class  createNewUserPage extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel createUser_label, twoTextFields,  twoButtons, container;
	private JLabel logInJLabel, nameLabel, passwordLabel;
	private JTextField username, password;
	private JButton  Create, Cancel;
	private String nameString;
	private String passwordString;
	private Connection conn;
	private Statement st;
	private ResultSet rs;
	private java.applet.AudioClip click = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/Do.WAV"));

	ImageIcon bg = new ImageIcon(getClass().getResource("/images/loginbg.jpg"));
	Image bgImage = bg.getImage();
	public class myPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		@Override
		protected void paintComponent(Graphics g) 
		{
		    super.paintComponent(g);
		        g.drawImage(bgImage, 0, 0, null);
		}
	}
	
	public createNewUserPage()
	{
		super("Pokemon");
		setSize(new Dimension(800, 660));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		container= new JPanel(); 
		container.setOpaque(false);
		container.setPreferredSize(new Dimension(800, 500));
		createUser_label = new JPanel();
		createUser_label.setPreferredSize(new Dimension(800, 250));
		ImageIcon icon = new ImageIcon(getClass().getResource("/images/Pokemon_logo.png"));
		logInJLabel = new JLabel(icon,SwingConstants.CENTER);
		logInJLabel.setPreferredSize(new Dimension(800, 250));
		createUser_label.add(logInJLabel);
		twoTextFields = new JPanel();
		twoTextFields.setPreferredSize(new Dimension(800, 100));
		ImageIcon name = new ImageIcon(getClass().getResource("/images/username.jpg"));
		nameLabel = new JLabel(name, SwingConstants.CENTER);
		ImageIcon pw = new ImageIcon(getClass().getResource("/images/password.jpg"));
		passwordLabel = new JLabel(pw, SwingConstants.CENTER);
		
		username = new JTextField();
		username.setFont(new Font("Arial", Font.PLAIN, 16));
		password = new JPasswordField();
		password.setFont(new Font("Arial", Font.PLAIN, 16));
		
        username.setPreferredSize(new Dimension(500, 30));
		password.setPreferredSize(new Dimension(500, 30));
		
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));
		p1.add(nameLabel);
		p1.add(passwordLabel);
		JPanel p2 = new JPanel();
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p2.add(username);
		p2.add(password);
		p1.setOpaque(false);
		p2.setOpaque(false);
		twoTextFields.add(p1);
		twoTextFields.add(p2);

		twoTextFields.setOpaque(false);
		twoButtons = new JPanel();
		twoButtons.setPreferredSize(new Dimension(800, 40));
		ImageIcon cr = new ImageIcon(getClass().getResource("/images/create.jpg"));
		Create = new JButton(cr);
		Create.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				click.play();
				//create new user
				nameString = username.getText();
				passwordString = password.getText();
				boolean valid = createUser ();
				if (valid) {
					setVisible (false);
				} else {
					
				}
				
            }
		});
		ImageIcon ca = new ImageIcon(getClass().getResource("/images/cancel.jpg"));
		Cancel = new JButton(ca);
		Cancel.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				click.play();
				setVisible(false);
            }
		});
		Create.setPreferredSize(new Dimension(150, 80));
		Cancel.setPreferredSize(new Dimension(150, 80));
		
		twoButtons.setLayout(new BoxLayout(twoButtons, BoxLayout.X_AXIS));
		twoButtons.add(Box.createGlue());
		twoButtons.add(Create);
		twoButtons.add(Box.createGlue());
		twoButtons.add(Cancel);
		twoButtons.add(Box.createGlue());	
	
		
		createUser_label.setOpaque(false);
		twoTextFields.setOpaque(false);
		twoButtons.setOpaque(false);
		myPanel bgPanel = new myPanel();
		bgPanel.add(createUser_label);
		bgPanel.add(twoTextFields);
		
		bgPanel.add(twoButtons);
		add(bgPanel,BorderLayout.CENTER); 
		
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private boolean createUser() 
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/PokemonDatabase?user=root");
			st = conn.createStatement();
			String sql = "SELECT * FROM User WHERE username=? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nameString);
			rs = ps.executeQuery();
			if (rs.next() == true) { // username already exists
				JOptionPane.showMessageDialog(createNewUserPage.this,"Username already exists!",
						"Error",JOptionPane.WARNING_MESSAGE);
				username.setText("");
				password.setText("");
				return false;
			}
			else if(nameString.equals("") || passwordString.equals(""))
			{
				JOptionPane.showMessageDialog(createNewUserPage.this,"Please enter valid information!",
						"Error",JOptionPane.WARNING_MESSAGE);
				username.setText("");
				password.setText("");
				return false;
			}
			else if (rs.next() == false) {
			    String query = " INSERT INTO User (username,password)"
			        + "VALUES (?, SHA(?));";
			    ps = conn.prepareStatement(query);
			    ps.setString (1,nameString);
			    ps.setString (2, passwordString);
			 
			    ps.execute();
			       
			    JOptionPane.showMessageDialog(createNewUserPage.this,"Successfully created new user!",
						"Success",JOptionPane.WARNING_MESSAGE);
				return true;
			}
			
			//no such username+password combination
			System.out.println("Fail!");
			return false; 
		} catch (SQLException sqle) {
			System.out.println ("SQLException: " + sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println ("ClassNotFoundException: " + cnfe.getMessage());
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (st != null) st.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return false;
	}
}