package guiInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

public class testServer {
	private Vector<ChatThread> ctVector = new Vector<ChatThread>();
	private ArrayList<ChatThread> battleRoom=new ArrayList<ChatThread>();
	private ArrayList<Integer> roomTurn=new ArrayList<Integer>();
	//private Vector<AttackThread> 
	Vector<Integer> room2people = new Vector <Integer> (10);
	Vector <Vector<ChatThread>> readyVector = new Vector <Vector<ChatThread>>(10);
	Vector <Vector<ChatThread>> localVector = new Vector <Vector<ChatThread>> (10);
	Vector <Vector<ChatThread>> localReadyVector = new Vector <Vector<ChatThread>> (10);
	Vector<Vector<String>> enteredRoom = new Vector<Vector<String> > (10);
	
	public testServer() {
		ServerSocket ss = null;
		try {
			for (int i= 0; i<10;i++) {
				Vector<ChatThread> tt = new Vector<ChatThread>();
				Vector<ChatThread> tsts = new Vector<ChatThread>();
				Vector<ChatThread> tttt = new Vector<ChatThread>();
				//String[] strstr = null;
				Vector<String> aa = new Vector<String> ();
				readyVector.add(tt);
				localVector.add(tsts);
				localReadyVector.add(tttt);
				room2people.add(0);
				enteredRoom.add(aa);
			}
			System.out.println("Starting Server");
			ss = new ServerSocket(6789);
			while (true) {
				System.out.println("Waiting for client to connect...");
				Socket s = ss.accept();
				System.out.println("Client " + s.getInetAddress() + ":" + s.getPort() + " connected");
				ChatThread ct = new ChatThread(s, this);
				ctVector.add(ct);
				ct.start();
			}
		} catch (IOException ioe) {
			System.out.println("IOE: " + ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe) {
					System.out.println("IOE closing ServerSocket: " + ioe.getMessage());
				}
			}
		}
	}
	public void removeChatThread(ChatThread ct) {
		ctVector.remove(ct);
	}
	public void sendMessageToClients(ChatThread ct, String str) {
		for (ChatThread ct1 : ctVector) {
			if (!ct.equals(ct1)) {
				ct1.sendMessage(str);
			}
		}
	}
	
	
	public void sendMessageToOpp (ChatThread ct, String str) {
		//str = "%"+roomNumber+ playername +": "+newChat.getText();
		int roomNum =  Character.getNumericValue(str.charAt(1));
		//System.out.println("Size of the local vector: "+localVector.get(roomNum).size());
		if (localVector.get(roomNum).size() < 2) {
			localVector.get(roomNum).add(ct);
		}
			
		str = str.substring(2);
		if (str.charAt(str.length()-1)!=':') {
		for (ChatThread ct1 : localVector.get(roomNum)) {
			//if (!ct.equals(ct1)) {
				ct1.sendMessage(str);
			//}
		}
		}
	}
	
	
	public void sendMessageToOppReaday (ChatThread ct, String str) {
		//str = "%"+roomNumber+ playername +": "+newChat.getText();
		int roomNum =  Character.getNumericValue(str.charAt(1));
		//System.out.println("Size of the local vector: "+localVector.get(roomNum).size());
		if (localReadyVector.get(roomNum).size() < 2) {
			localReadyVector.get(roomNum).add(ct);
		}
			
		str = str.substring(2);
		if (str.charAt(str.length()-1)!=':') {
			for (ChatThread ct1 : localReadyVector.get(roomNum)) {
				//if (!ct.equals(ct1)) {
					ct1.sendMessage(str);
				//}
			}
		}
	}
	
	
	
	public void sendSkill(ChatThread ct, String str, boolean isSwitch)
	{
		String[] line = str.split("\t");
		int x = 0;
		if (isSwitch) {
			x = line[2].charAt(0)-48;
		} else {
			x = line[1].charAt(0)-48;
		}
		System.out.println("the x is: "+x);
		if(roomTurn.get(x)==0)
		{
			roomTurn.set(x,1);
		}
		else if(roomTurn.get(x)==1)
		{
			roomTurn.set(x,0);
		}
		
		if (isSwitch) {
			//the player switched a pokemon: "* \t switch \t roomNumber \t count"
			battleRoom.get(roomTurn.get(x)).sendMessage("**" + line[3]);
		}
		else {
			battleRoom.get(roomTurn.get(x)).sendMessage("*"+line[2]);
			//battleRoom.get(roomTurn.get(x)).sendMessage('*'+str.charAt(2)+"");
		}
	}
	public void checkRoom (ChatThread ct, String[] str){
		// line = # roomNumber playerName pkm1 pkm2 pkm3
		int x = Integer.parseInt(str[1]);
		readyVector.get(x).add(ct);
		int value = room2people.get(x);
		room2people.set(x, value+1);
		value = room2people.get(x);
		if (value == 2) {
			System.out.println(readyVector.get(x).size());
			
			String[] message0 = readyVector.get(x).get(0).readyMessage;
			String[] message1 = readyVector.get(x).get(1).readyMessage;
			System.out.println("bothReady\t" + message0[3] + "\t" + message0[4] + "\t" + message0[5] );
			System.out.println("bothReady\t" + message1[3] + "\t" + message1[4] + "\t" + message1[5]);
			readyVector.get(x).get(0).sendMessage("bothReady\t" + message1[3] + "\t" + message1[4] + "\t" + message1[5] );
			readyVector.get(x).get(1).sendMessage("bothReady\t" + message0[3] + "\t" + message0[4] + "\t" + message0[5] );
			/*
			for (ChatThread ct1 : readyVector.get(x)) {
				//if (!ct.equals(ct1)) {
					ct1.sendMessage("bothReady");
				//}
			}*/
		}
		
	}
	class ChatThread extends Thread {
		private BufferedReader br;
		private PrintWriter pw;
		private testServer cs;
		private Socket s;
		public String[] readyMessage;
		public ChatThread(Socket s, testServer cs) {
			this.cs = cs;
			this.s = s;
			try {
				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				pw = new PrintWriter(s.getOutputStream());
			} catch (IOException ioe) {
				System.out.println("IOE in ChatThread constructor: " + ioe.getMessage());
			}
		}

		public void sendMessage(String str) {
			pw.println(str);
			pw.flush();
		}

		public void run() {
			try {
				while (true) {
					String line = br.readLine();
					if (line != null && line != "null") {
						if (line.charAt(0) == '#') {
							String[] input = line.split("\t");
							this.readyMessage = input;
							cs.checkRoom(this,input);
						}
						else if (line.charAt(0) == '$') {
							System.out.println("from client: "+line);
							String message = "";
							for (int i = 0;i<5;i++) {
								if (enteredRoom.get(i).size() == 0) {
									message += "No player";
								} else {
									for (String s : enteredRoom.get(i)) {
										message += s;
										message+= " ";
									}
								}
								message+="\t";
								
							}
							System.out.println("Message to send: "+message);
							sendMessage(message);
						}
						else if(line.charAt(0)=='&')
						{
							battleRoom.add(this);
							roomTurn.add(0);
							roomTurn.add(0);
							roomTurn.add(0);
							roomTurn.add(0);
							roomTurn.add(0);
							roomTurn.add(0);
							//0-1,0
							//2-3,1
							//4-5,2
							//5-6,3
							
						}
						else if (line.charAt(0)=='*')
						{
							if (line.split("\t")[1].equals("switch")) {
								cs.sendSkill(this, line, true);
							} else {
								cs.sendSkill(this, line, false);
							}
							
						}
						else if (line.charAt(0) == '%') {
							cs.sendMessageToOpp(this, line);
						}
						else if (line.charAt(0) == '^') {
							//"^"+roomNumber+ playername +": "+newChat.getText()
							int roomNum = Character.getNumericValue(line.charAt(1));
							String name = line.substring(2, line.indexOf(':'));
							boolean existed =false;
							for (String s : enteredRoom.get(roomNum)) {
								if (s.equals(name)){
									existed = true;
								}	
							}
							if (existed == false) {
							enteredRoom.get(roomNum).add(name);
							}
							cs.sendMessageToOppReaday(this, line);
						}
						else
						{
							cs.sendMessageToClients(this, line);
						}
					}
				}
			} catch (IOException ioe) {
				cs.removeChatThread(this);
				System.out.println(s.getInetAddress() + ":" + s.getPort() + " disconnected.");
			}
		}
	}

	public static void main(String [] args) {
		new testServer();
	}
}




