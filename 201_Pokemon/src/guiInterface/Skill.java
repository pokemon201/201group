package guiInterface;

import java.io.Serializable;

public class Skill implements Serializable
{

	private static final long serialVersionUID = 1L;
	private String name;
	private String type; 
	private int damage;
	private int mp;
	public Skill(String name, String type, int damage,int mp)
	{
		this.name=name;
		this.type=type;
		this.damage=damage;
		this.mp=mp;
	}
	public String getType()
	{
		return type;
	}
	
	public String getName()
	{
		return name;
	}
	public int getMp()
	{
		return mp;
	
	}
	public int getDmg()
	{
		return damage;
	}
}
