package guiInterface;

public class User {
	private String username;
	private int[] pokemons;
	
	public User( String username, int p1, int p2, int p3 ) {
		this.username = username;
		pokemons = new int[3];
		pokemons[0] = p1;
		pokemons[1] = p2;
		pokemons[2] = p3;
	}
	
	public String getUsername() {
		return username;
	}
	
	public int[] getPokemons() {
		return pokemons;
	}
}
