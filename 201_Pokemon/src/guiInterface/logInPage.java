package guiInterface;

import java.awt.*;
import java.util.Random;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.*;

public class logInPage extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel logIn_label, twoTextFields, threeButtons;
	private JLabel logInJLabel, nameLabel, passwordLabel;
	private JTextField username;
	private JPasswordField password;
	private JButton logIn, newUser, playAsGuest;
	private String nameString;
	private String passwordString;
	private Connection conn;
	private Statement st;
	private ResultSet rs;
	private boolean guest;
	private java.applet.AudioClip click = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/Do.WAV"));
	private java.applet.AudioClip opening = java.applet.Applet.newAudioClip(getClass().getResource("/sounds/101-opening.wav"));

	private final ImageIcon pokemonIcons[];
	{
		pokemonIcons = new ImageIcon[20];
		pokemonIcons[0] = new ImageIcon(getClass().getResource("/images/1.png"));
		pokemonIcons[1] = new ImageIcon(getClass().getResource("/images/2.png"));
		pokemonIcons[2] = new ImageIcon(getClass().getResource("/images/3.png"));
		pokemonIcons[3] = new ImageIcon(getClass().getResource("/images/4.png"));
		pokemonIcons[4] = new ImageIcon(getClass().getResource("/images/5.png"));
		pokemonIcons[5] = new ImageIcon(getClass().getResource("/images/6.png"));
		pokemonIcons[6] = new ImageIcon(getClass().getResource("/images/7.png"));
		pokemonIcons[7] = new ImageIcon(getClass().getResource("/images/8.png"));
		pokemonIcons[8] = new ImageIcon(getClass().getResource("/images/9.png"));
		pokemonIcons[9] = new ImageIcon(getClass().getResource("/images/10.png"));
		pokemonIcons[10] = new ImageIcon(getClass().getResource("/images/11.png"));
		pokemonIcons[11] = new ImageIcon(getClass().getResource("/images/12.png"));
		pokemonIcons[12] = new ImageIcon(getClass().getResource("/images/13.png"));
		pokemonIcons[13] = new ImageIcon(getClass().getResource("/images/14.png"));
		pokemonIcons[14] = new ImageIcon(getClass().getResource("/images/15.png"));
		pokemonIcons[15] = new ImageIcon(getClass().getResource("/images/16.png"));
		pokemonIcons[16] = new ImageIcon(getClass().getResource("/images/17.png"));
		pokemonIcons[17] = new ImageIcon(getClass().getResource("/images/18.png"));
		pokemonIcons[18] = new ImageIcon(getClass().getResource("/images/19.png"));
		pokemonIcons[19] = new ImageIcon(getClass().getResource("/images/20.png"));
	}
	
	ImageIcon bg = new ImageIcon(getClass().getResource("/images/loginbg.jpg"));
	Image bgImage = bg.getImage();
	public class myPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		@Override
		protected void paintComponent(Graphics g) 
		{
		    super.paintComponent(g);
		        g.drawImage(bgImage, 0, 0, null);
		}
	}
	public logInPage() 
	{
		super("Pokemon");
		opening.play();
		setSize(new Dimension(800, 660));
		setResizable(false);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		logIn_label = new JPanel();
		logIn_label.setPreferredSize(new Dimension(800, 150));
		ImageIcon icon = new ImageIcon(getClass().getResource("/images/Pokemon_logo.png"));
		logInJLabel = new JLabel(icon,SwingConstants.CENTER);
		logInJLabel.setPreferredSize(new Dimension(800, 250));
		logIn_label.add(logInJLabel);
		twoTextFields = new JPanel();
		twoTextFields.setPreferredSize(new Dimension(800, 100));
		
		ImageIcon name = new ImageIcon(getClass().getResource("/images/username.jpg"));
		nameLabel = new JLabel(name, SwingConstants.CENTER);
		ImageIcon pw = new ImageIcon(getClass().getResource("/images/password.jpg"));
		passwordLabel = new JLabel(pw, SwingConstants.CENTER);
		
		username = new JTextField();
		username.setFont(new Font("Arial", Font.PLAIN, 16));
		password = new JPasswordField();
		password.setFont(new Font("Arial", Font.PLAIN, 16));
		
        username.setPreferredSize(new Dimension(500, 30));
		password.setPreferredSize(new Dimension(500, 30));
		
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));
		p1.add(nameLabel);
		p1.add(passwordLabel);
		JPanel p2 = new JPanel();
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p2.add(username);
		p2.add(password);
		p1.setOpaque(false);
		p2.setOpaque(false);
		twoTextFields.add(p1);
		twoTextFields.add(p2);
		
		
		twoTextFields.setOpaque(false);
		
		ImageIcon start = new ImageIcon(getClass().getResource("/images/start.jpg"));
		logIn = new JButton(start);
		logIn.addMouseListener(new MouseAdapter()
		{
			@SuppressWarnings("deprecation")
			public void mouseClicked(MouseEvent evt)
            {
				//verify user	
				opening.stop();
				click.play();
				nameString = username.getText();
				passwordString = password.getText();
				boolean valid = authenticate();
				if (valid) 
				{
					//TODO: if user credentials valid, switch to select pokemon screen
					//TODO: if don't setVisible(false), user can still switch to log in page
					String playername = username.getText();
					new Team(playername);
					setVisible(false);
				} 
				else 
				{
					//TODO: if not valid, display error message
					Object[] options = {"Create New User", "Return"};
					int selection = JOptionPane.showOptionDialog(logInPage.this, 
							"Invalid username or password.","Login Failed",JOptionPane.YES_NO_OPTION,
							JOptionPane.ERROR_MESSAGE,null,options,options[0]);
					if (selection == 0) {
						new createNewUserPage ();
						setVisible (false);
					} else if (selection == 1 ) {
						username.setText("");
						password.setText("");
					}
				}
            }
		});
		logIn.setPreferredSize(new Dimension(150, 80));
		threeButtons = new JPanel();
		threeButtons.setOpaque(false);
		threeButtons.setPreferredSize(new Dimension(800, 40));
		ImageIcon nu = new ImageIcon(getClass().getResource("/images/newuser.jpg"));
		newUser = new JButton(nu);
		ImageIcon pag = new ImageIcon(getClass().getResource("/images/guest.jpg"));
		playAsGuest = new JButton(pag);
		playAsGuest.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				opening.stop();
				click.play();
				//TODO: randomly select three pokemons from database
				ArrayList<Pokemon> pkm = new ArrayList<Pokemon>();
				Skill tmpS1[]=new Skill[4];
				tmpS1[0]=new Skill("Attack","Normal",5,0);
				tmpS1[1]=new Skill("Scratch","grass",12,6);
				tmpS1[2]=new Skill("Psychic","grass",11,12);
				tmpS1[3]=new Skill("Hidden Grass","Grass",14,11);
				Skill tmpS2[]=new Skill[4];
				tmpS2[0]=new Skill("Attack","Normal",5,0);
				tmpS2[1]=new Skill("Flare Blitz","Fire",7,6);
				tmpS2[2]=new Skill("Solar Beam","Fire",6,5);
				tmpS2[3]=new Skill("Hidden Fire","Fire",14,13);
				Skill tmpS3[]=new Skill[4];
				tmpS3[0]=new Skill("Attack","Normal",5,0);
				tmpS3[1]=new Skill("Headbutt","Ground",11,3);
				tmpS3[2]=new Skill("Earthquake","Ground",12,3);
				tmpS3[3]=new Skill("Slam","Ground",13,2);
				Pokemon p1 = new Pokemon("Exeggcute",60,135,10,"Grass",tmpS1,pokemonIcons[10], 11);
				Pokemon p2 = new Pokemon("Charmander",39,100,10,"Fire",tmpS2,pokemonIcons[0], 1);
				Pokemon p3 = new Pokemon("Cubone",50,125,10,"Ground",tmpS3,pokemonIcons[18], 19);
				pkm.add(p1);
				pkm.add(p2);
				pkm.add(p3);
				guest = true;
				//String playername = username.getText();
				Random rand = new Random();
				int r = rand.nextInt(1000)+1;
				String playername = "guest" + r; 
				setVisible(false);
				dispose();
				new Rooms(pkm,guest,playername);
				
            }
		});
		newUser.setPreferredSize(new Dimension(150, 80));
		newUser.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent evt)
            {
				opening.stop();
				click.play();
				//TODO: if not setVisible(false), user can still see login page
				new createNewUserPage();
            }
		});
		playAsGuest.setPreferredSize(new Dimension(150, 80));
		threeButtons.setLayout(new BoxLayout(threeButtons, BoxLayout.X_AXIS));
		threeButtons.add(Box.createGlue());
		threeButtons.add(newUser);
		threeButtons.add(Box.createGlue());
		threeButtons.add(logIn);
		threeButtons.add(Box.createGlue());
		threeButtons.add(playAsGuest);
		threeButtons.add(Box.createGlue());

		myPanel bgPanel = new myPanel();
		bgPanel.add(logInJLabel);
		bgPanel.add(twoTextFields);
		bgPanel.add(threeButtons);
		add(bgPanel);

		setLocationRelativeTo(null);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private boolean authenticate() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/PokemonDatabase?user=root");
			st = conn.createStatement();
			String sql = "SELECT * FROM User WHERE username=? AND password=SHA(?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nameString);
			ps.setString(2, passwordString);
			rs = ps.executeQuery();
			while (rs.next()) {
				//username and password pair correct
				//String pw = rs.getString("password");
				//System.out.println("Success! Password = " + pw);
				guest = false;
				return true;
			}
			//no such username+password combination
			System.out.println("Fail!");
			return false; 
		} catch (SQLException sqle) {
			System.out.println ("SQLException: " + sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println ("ClassNotFoundException: " + cnfe.getMessage());
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (st != null) st.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return false;
	}	

	// /main method
	public static void main(String[] args) 
	{
		new logInPage();
	}
}
